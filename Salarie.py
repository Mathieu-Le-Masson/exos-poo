class Salarie:
    def __init__(self, salaire_mensuel, employeur=None, voiture_de_fonction=None, voiture_perso=True, directeur=False):
        self.salaire_mensuel = salaire_mensuel
        self.employeur = employeur
        self.voiture_de_fonction = voiture_de_fonction
        self.voiture_perso = voiture_perso
        self.directeur = directeur

    def set_employeur(self, new_employeur):
        if not self.employeur:
            self.employeur = new_employeur
        else:
            print("Ce salarié  a déjà un employeur")
        return self.employeur

    def augmenter(self, salarie, nouveau_salaire):
        if self.directeur:
            salarie.salaire_mensuel = nouveau_salaire
        else:
            print("AcessRightError")
