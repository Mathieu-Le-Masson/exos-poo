from Entreprise import Entreprise
from Salarie import Salarie
from Bureau import Bureau
from Voiture import VoitureFonction
from Simulation import Simulation
import pydoc


if __name__ == "__main__":

    directeur1 = Salarie(5000, directeur=True)
    salarie1 = Salarie(30000)
    salarie2 = Salarie(10000)
    salarie3 = Salarie(12000)
    salarie4 = Salarie(16000)
    directeur2 = Salarie(40000, directeur=True)
    salarie5 = Salarie(15000)
    salarie6 = Salarie(22000)
    salarie7 = Salarie(27000)
    salarie8 = Salarie(14000)
    salarie9 = Salarie(13000)
    salarie10 = Salarie(16000)
    salarie11 = Salarie(34000)
    salarie12 = Salarie(17000)

    bureau1 = Bureau(150000)
    bureau2 = Bureau(250000)
    bureau3 = Bureau(350000)
    bureau4 = Bureau(100000)
    bureau5 = Bureau(400000)
    bureau6 = Bureau(460000)
    bureau7 = Bureau(320000)
    bureau8 = Bureau(840000)
    bureau9 = Bureau(750000)
    bureau10 = Bureau(390000)

    entreprise1 = Entreprise([], [], [], [], 1000000, directeur1)
    entreprise2 = Entreprise([], [], [], [], 2000000, directeur2)
    liste_entreprises = [entreprise1, entreprise2]
    liste_entreprises_str = ["entreprise1", "entreprise2"]

    voiture_fn1 = VoitureFonction(None, 38000, entreprise1, 1000)
    voiture_fn2 = VoitureFonction(None, 12000, entreprise1, 1000)
    voiture_fn3 = VoitureFonction(None, 23000, entreprise1, 2000)
    voiture_fn4 = VoitureFonction(None, 45000, entreprise2, 3500)
    voiture_fn5 = VoitureFonction(None, 8000, entreprise2, 1000)
    voiture_fn6 = VoitureFonction(None, 17000, entreprise2, 1500)
    voiture_fn7 = VoitureFonction(None, 28000, None, 2500)
    voiture_fn8 = VoitureFonction(None, 16000, None, 1500)
    voiture_fn9 = VoitureFonction(None, 20000, None, 2000)
    voiture_fn10 = VoitureFonction(None, 15000, None, 1500)
    voiture_fn11 = VoitureFonction(None, 16000, None, 1500)
    voiture_fn12 = VoitureFonction(None, 9000, None, 1000)

    entreprise1.set_salaries([directeur1, salarie1, salarie2, salarie3, salarie4])
    entreprise1.set_voitures_de_fonction([voiture_fn1, voiture_fn2, voiture_fn3])
    entreprise1.set_voitures_de_fonction_libres([voiture_fn1, voiture_fn2, voiture_fn3])
    entreprise1.set_bureaux([bureau1, bureau2, bureau3])

    entreprise2.set_salaries([directeur2, salarie5, salarie6, salarie7, salarie8])
    entreprise2.set_voitures_de_fonction([voiture_fn4, voiture_fn5, voiture_fn6])
    entreprise2.set_voitures_de_fonction_libres([voiture_fn4, voiture_fn5, voiture_fn6])
    entreprise2.set_bureaux([bureau4, bureau5, bureau6])

    entreprise1.attribuer_voiture_fn(directeur1, voiture_fn1)
    entreprise1.attribuer_voiture_fn(salarie1, voiture_fn2)
    entreprise1.attribuer_voiture_fn(salarie3, voiture_fn3)

    entreprise2.attribuer_voiture_fn(directeur2, voiture_fn4)
    entreprise2.attribuer_voiture_fn(salarie6, voiture_fn5)
    entreprise2.attribuer_voiture_fn(salarie7, voiture_fn6)

    All_salaries = [directeur1, directeur2, salarie1, salarie2, salarie3, salarie4, salarie5, salarie6, salarie7,
                    salarie8, salarie9, salarie10, salarie11, salarie12]

    All_bureaux = [bureau1, bureau2, bureau3, bureau4, bureau5, bureau6, bureau7, bureau8, bureau9,
                   bureau10]

    All_voitures = [voiture_fn1, voiture_fn2, voiture_fn9, voiture_fn4, voiture_fn5,
                    voiture_fn6, voiture_fn7, voiture_fn8, voiture_fn9, voiture_fn10, voiture_fn11, voiture_fn12]

    """
    On fait un petit menu pour lancer la simulation
    L'user choisit l'entreprise voulue et le nombre de mois sur lequel run la simulation
    """
    continuer = True
    while continuer:
        try:
            nb_mois = int(input("Saisissez le nombre de mois entre 1 et 12 compris souhaité : "))
            if 0 < nb_mois < 13:
                print(liste_entreprises_str)
                choix = int(input("Entrez la position de l'entreprise voulue : "))
                # si choix in liste_entreprises :
                simulation = Simulation(liste_entreprises[choix - 1], nb_mois)
                simulation.lancer_simulation(All_salaries, All_voitures, All_bureaux)
                y = input("Souhaitez-vous continuer ? [y] ou [n]")
                if y == "y" or y == "Y":
                    continuer = True
                else:
                    exit()

            else:
                error_message = "PeriodOutOfScopeError : Le choix doit être entre 1 et 12"
                print(f'\033[91m{error_message}\033[0m')  # Affichage rouge
                input("Appuyer sur entrée pour continuer")
                continuer = True

        except ValueError as e:
            print(e)
            error_message = "Valeur attendue : entier entre 1 et 12 compris"
            print(f'\033[91m{error_message}\033[0m')  # Affichage rouge
            input("Appuyer sur entrée pour continuer")
