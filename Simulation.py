import numpy as np
import matplotlib.pyplot as plt
import random


class Simulation:
    def __init__(self, entreprise, nb_mois):
        self.entreprise = entreprise
        self.nb_mois = nb_mois

    def lancer_simulation(self, salaries, voitures, bureaux):
        """
        Lance une simulation de la vie de l'entreprise
        :param salaries: pool de salariés
        :param voitures: pool de voitures
        :param bureaux: pool de bureaux
        :return:
        """
        if 0 < self.nb_mois < 13:
            i = 1
            liste_mois = [0]
            valeur_biens_entreprise = [0]      # On calcule la valeur des biens au mois 0
            for k in self.entreprise.bureaux:
                valeur_biens_entreprise[0] += k.prix
            for k in self.entreprise.voitures_fonction:
                valeur_biens_entreprise[0] += k.prix
            print(valeur_biens_entreprise)
            charges_mensuelles_entreprise = [0]      # Valeur des charges au mois 0
            for k in self.entreprise.salaries:
                charges_mensuelles_entreprise[0] += k.salaire_mensuel
            for k in self.entreprise.voitures_fonction:
                charges_mensuelles_entreprise[0] += k.cout_mensuel
            print(charges_mensuelles_entreprise)
            while i <= self.nb_mois:            # Une boucle pour chaque mois
                for _ in range(2):              # 2 Opérations par mois
                    nb_random = random.randint(0, 5)
                    if nb_random == 0:
                        self.entreprise.embaucher(random.choice(salaries))
                    elif nb_random == 1:
                        if len(self.entreprise.salaries) != 0:
                            self.entreprise.virer(random.choice(self.entreprise.salaries))
                    elif nb_random == 2:
                        self.entreprise.acheter_voiture(random.choice(voitures))
                    elif nb_random == 3:
                        if len(self.entreprise.voitures_fonction) != 0:
                            self.entreprise.vendre_voiture(random.choice(self.entreprise.voitures_fonction))
                    elif nb_random == 4:
                        self.entreprise.acheter_bureau(random.choice(bureaux))
                    elif nb_random == 5:
                        if len(self.entreprise.bureaux) != 0:
                            self.entreprise.vendre_bureau(random.choice(self.entreprise.bureaux))
                liste_mois.append(str(i))           # On update la liste des mois
                valeur_biens_entreprise.append(0)
                for k in self.entreprise.bureaux:       # On update la liste des valeurs
                    valeur_biens_entreprise[i] += k.prix
                for k in self.entreprise.voitures_fonction:
                    valeur_biens_entreprise[i] += k.prix
                print(valeur_biens_entreprise)
                charges_mensuelles_entreprise.append(0)     # On update la liste des charges
                for k in self.entreprise.salaries:
                    charges_mensuelles_entreprise[i] += k.salaire_mensuel
                for k in self.entreprise.voitures_fonction:
                    charges_mensuelles_entreprise[i] += k.cout_mensuel
                print(charges_mensuelles_entreprise)
                i = i + 1

            x = liste_mois                  # On représente les données aves un graphique
            valeur = valeur_biens_entreprise
            charges = charges_mensuelles_entreprise

            x_axis = np.arange(len(x))

            plt.bar(x_axis - 0.2, valeur, 0.4, label='Valeur')
            plt.bar(x_axis + 0.2, charges, 0.4, label='Charges')

            plt.xticks(x_axis, x)
            plt.xlabel("Mois")
            plt.ylabel("Montant")
            plt.title("Valeur des biens et charges mensuelles de l'entreprise")
            plt.legend()
            plt.show()

        else:
            print("PeriodOutOfScopeError")
        return
