class Entreprise:
    def __init__(self, salaries, bureaux, voitures_fonction, voiture_fonction_libres, chiffre_affaires, directeur):
        self.salaries = salaries
        self.bureaux = bureaux
        self.voitures_fonction = [voitures_fonction]
        self.voitures_fonction_libres = [voiture_fonction_libres]
        self.chiffre_affaires = chiffre_affaires
        self.directeur = directeur

    def set_salaries(self, liste_salaries):
        self.salaries = liste_salaries
        for k in liste_salaries:
            k.employeur = self
        return self.salaries

    def embaucher(self, nouvel_employe):
        if not nouvel_employe.employeur:
            self.salaries.append(nouvel_employe)
            if len(self.voitures_fonction_libres) > 0:
                nouvel_employe.voiture_de_fonction = self.voitures_fonction_libres[0]
        else:
            "Ce salarié a déjà un employeur"
        return self.salaries

    def debaucher(self, employe_concurrence, entreprise_concurrente):
        if employe_concurrence.employeur:
            self.salaries.append(employe_concurrence)
            entreprise_concurrente.salaries.remove(employe_concurrence)
            employe_concurrence.voiture_de_fonction.user = None
            entreprise_concurrente.voitures_fonction_libres.append(employe_concurrence.voiture_de_fonction)
            employe_concurrence.voiture_de_fonction = None
        return self.salaries

    def virer(self, employe_vire):
        self.salaries.remove(employe_vire)
        employe_vire.employeur = None
        if employe_vire.voiture_de_fonction is not None:
            self.voitures_fonction_libres.append(employe_vire.voiture_de_fonction)
            employe_vire.voiture_de_fonction.user = None
            employe_vire.voiture_de_fonction = None
        return self.salaries

    def acheter_voiture(self, voiture_achetee):
        if voiture_achetee.owner is None:
            self.voitures_fonction.append(voiture_achetee)
            self.voitures_fonction_libres.append(voiture_achetee)
            voiture_achetee.owner = self
        else:
            print("La voiture que vous essayez d'acheter n'est pas disponible !")
        return self.voitures_fonction

    def set_voitures_de_fonction(self, liste_voiture_fn):
        self.voitures_fonction = liste_voiture_fn
        for k in liste_voiture_fn:
            k.owner = self
        return self.voitures_fonction

    def set_voitures_de_fonction_libres(self, liste_voiture_fn_libres):
        self.voitures_fonction_libres = liste_voiture_fn_libres
        return self.voitures_fonction_libres

    def vendre_voiture(self, voiture_vendue):
        if voiture_vendue.user:
            voiture_vendue.user.voiture_de_fonction = None
            voiture_vendue.user = None
        self.voitures_fonction.remove(voiture_vendue)
        voiture_vendue.owner = None
        return self.voitures_fonction

    def attribuer_voiture_fn(self, salarie, voiture_attribuee):
        if not salarie.voiture_de_fonction:
            if voiture_attribuee.owner == self:
                if not voiture_attribuee.user:
                    voiture_attribuee.user = salarie
                    salarie.voiture_de_fonction = voiture_attribuee
                    self.voitures_fonction_libres.remove(voiture_attribuee)
                else:
                    print("Cette voiture est déjà attribuée")
            else:
                print("Cette voiture n'appartient pas à l'entreprise")
        else:
            print("Ce salarié a déjà une voiture de fonction")
        return self.voitures_fonction

    def acheter_bureau(self, bureau_achete):
        if not bureau_achete.owner:
            if not bureau_achete.occupation:
                self.bureaux.append(bureau_achete)
                bureau_achete.owner = self
            else:
                print("Le bureau que vous essayez d'acheter est occupé !")
        else:
            print("Ce bureau appartient déjà à quelqu'un !")
        return self.bureaux

    def vendre_bureau(self, bureau_vendu):
        if bureau_vendu.owner == self:
            self.bureaux.remove(bureau_vendu)
            bureau_vendu.occupation = False
        else:
            print("Ce bureau n'est pas à vous !")
        return self.bureaux

    def set_bureaux(self, liste_bureaux):
        self.bureaux = liste_bureaux
        for k in liste_bureaux:
            k.owner = self
            k.occupation = True
        return self.bureaux
