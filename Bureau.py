class Bureau:
    def __init__(self, prix, owner=None, occupation=False):
        self.prix = prix
        self.owner = owner
        self.occupation = occupation

    def get_occupation(self):
        if self.occupation:
            print("Bureau occupé")
        else:
            print("Bureau vide")

    def set_owner(self, new_owner):
        self.owner = new_owner
        return self.owner
